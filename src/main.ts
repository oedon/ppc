document.getElementById('exec2').onclick = function (): void {
  const vIn: HTMLInputElement = document.getElementById('vIn') as HTMLInputElement
  const sIn: HTMLInputElement = document.getElementById('sIn') as HTMLInputElement
  const tIn: HTMLInputElement = document.getElementById('tIn') as HTMLInputElement

  const v: number = Number(vIn.value)
  const s: number = Number(sIn.value)
  const t: number = Number(tIn.value)

  const vOut: HTMLInputElement = document.getElementById('vOut') as HTMLInputElement
  const sOut: HTMLInputElement = document.getElementById('sOut') as HTMLInputElement
  const tOut: HTMLInputElement = document.getElementById('tOut') as HTMLInputElement

    if (isNaN(v) || v === 0) {
        console.log("test")
    }

    if (v !== 0 && s !== 0 && t !== 0) {
        vOut.innerHTML = "ERROR"
        sOut.innerHTML = "ERROR"
        tOut.innerHTML = "ERROR"
    }
    else if (v === 0 && s === 0 && t === 0) {
        vOut.innerHTML = "NO INPUT"
        sOut.innerHTML = "NO INPUT"
        tOut.innerHTML = "NO INPUT"
    }
    else if (s !== 0 && t !== 0) {
        vOut.innerHTML = getSpeed(s, t) + 'km/h'
        sOut.innerHTML = ''
        tOut.innerHTML = ''
    }
    else if (v !== 0 && t !== 0) {
        vOut.innerHTML = ''
        sOut.innerHTML = getDistance(v, t) + 'm'
        tOut.innerHTML = ''
    }
    else if (v !== 0 && s !== 0) {
        vOut.innerHTML = ''
        sOut.innerHTML = ''
        tOut.innerHTML = getTime(s, v) + 's'
    }
    

  
  console.log(v, s, t, typeof v, typeof s, typeof t)
}

document.getElementById('reset2').onclick = function (): void {
  let vIn: HTMLInputElement = document.getElementById('vIn') as HTMLInputElement
  let sIn: HTMLInputElement = document.getElementById('sIn') as HTMLInputElement
  let tIn: HTMLInputElement = document.getElementById('tIn') as HTMLInputElement

  let vOut: HTMLInputElement = document.getElementById('vOut') as HTMLInputElement
  let sOut: HTMLInputElement = document.getElementById('sOut') as HTMLInputElement
  let tOut: HTMLInputElement = document.getElementById('tOut') as HTMLInputElement

  vIn.value = ''
  sIn.value = ''
  tIn.value = ''
  vOut.innerHTML = ''
  sOut.innerHTML = ''
  tOut.innerHTML = ''
}

function getSpeed(s: number, t: number): string {
    return ((s / t) * 3.6).toFixed(2);
}
function getDistance(v: number, t: number): string {
    return ((v / 3.6) * t).toFixed(2);
}
function getTime(s: number, v: number): string {
    return ( s / (v / 3.6)).toFixed(2);
}


// https://hades.website-eu-central-1.linodeobjects.com/
