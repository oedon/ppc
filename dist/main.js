document.getElementById('exec2').onclick = function () {
    var vIn = document.getElementById('vIn');
    var sIn = document.getElementById('sIn');
    var tIn = document.getElementById('tIn');
    var v = Number(vIn.value);
    var s = Number(sIn.value);
    var t = Number(tIn.value);
    var vOut = document.getElementById('vOut');
    var sOut = document.getElementById('sOut');
    var tOut = document.getElementById('tOut');
    if (isNaN(v) || v === 0) {
        console.log("test");
    }
    if (v !== 0 && s !== 0 && t !== 0) {
        vOut.innerHTML = "ERROR";
        sOut.innerHTML = "ERROR";
        tOut.innerHTML = "ERROR";
    }
    else if (v === 0 && s === 0 && t === 0) {
        vOut.innerHTML = "NO INPUT";
        sOut.innerHTML = "NO INPUT";
        tOut.innerHTML = "NO INPUT";
    }
    else if (s !== 0 && t !== 0) {
        vOut.innerHTML = getSpeed(s, t) + 'km/h';
        sOut.innerHTML = '';
        tOut.innerHTML = '';
    }
    else if (v !== 0 && t !== 0) {
        vOut.innerHTML = '';
        sOut.innerHTML = getDistance(v, t) + 'm';
        tOut.innerHTML = '';
    }
    else if (v !== 0 && s !== 0) {
        vOut.innerHTML = '';
        sOut.innerHTML = '';
        tOut.innerHTML = getTime(s, v) + 's';
    }
    console.log(v, s, t, typeof v, typeof s, typeof t);
};
document.getElementById('reset2').onclick = function () {
    var vIn = document.getElementById('vIn');
    var sIn = document.getElementById('sIn');
    var tIn = document.getElementById('tIn');
    var vOut = document.getElementById('vOut');
    var sOut = document.getElementById('sOut');
    var tOut = document.getElementById('tOut');
    vIn.value = '';
    sIn.value = '';
    tIn.value = '';
    vOut.innerHTML = '';
    sOut.innerHTML = '';
    tOut.innerHTML = '';
};
function getSpeed(s, t) {
    return ((s / t) * 3.6).toFixed(2);
}
function getDistance(v, t) {
    return ((v / 3.6) * t).toFixed(2);
}
function getTime(s, v) {
    return (s / (v / 3.6)).toFixed(2);
}
// https://hades.website-eu-central-1.linodeobjects.com/
